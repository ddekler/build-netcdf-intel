#!/bin/bash

if [ $# -eq 1 ]; then
    C_INSTALL_PREFIX=$1 
    FORTRAN_INSTALL_PREFIX=$1 
elif [ $# -eq 2 ]; then
    C_INSTALL_PREFIX=$1 
    FORTRAN_INSTALL_PREFIX=$2
else
    echo "If installing both libraries to the same location, the install directory should be specified as the first argument."
    echo "For example:"
    echo "  $ ./build-netcdf-intel.sh ~/.local"
    echo "will install NetCDF in your home directory."
    echo "If installing to separate locations, the NetCDF-C directory should be specified as the first argument."
    echo "NetCDF-Fortran location as the second."
    echo "For example:"
    echo "  $ ./build-netcdf-intel.sh some/path/NetCDF-C some/path/NetCDF-Fortran"
    exit 1
fi

BUILD_DIR=build
NETCDF_C_VER=4.9.2
NETCDF_FORTRAN_VER=4.6.1
NETCDF_C_DIR=netcdf-c-$NETCDF_C_VER
NETCDF_FORTRAN_DIR=netcdf-fortran-$NETCDF_FORTRAN_VER
NETCDF_C_TAR=$NETCDF_C_DIR.tar.gz
NETCDF_FORTRAN_TAR=$NETCDF_FORTRAN_DIR.tar.gz

mkdir -p $BUILD_DIR
cd $BUILD_DIR

## Temp file to store urls
if [ -f netcdf_urls ]; then
    rm netcdf_urls
fi

## Add netcdf-c to download urls
if [ ! -f $NETCDF_C_TAR ]; then
    echo "https://downloads.unidata.ucar.edu/netcdf-c/$NETCDF_C_VER/$NETCDF_C_TAR" >> netcdf_urls
fi

## Add netcdf-fortran to download urls
if [ ! -f $NETCDF_FORTRAN_TAR ]; then
    echo "https://downloads.unidata.ucar.edu/netcdf-fortran/$NETCDF_FORTRAN_VER/$NETCDF_FORTRAN_TAR" >> netcdf_urls
fi

## Start the download
if [ -f netcdf_urls ]; then
    wget --show-progress -i netcdf_urls
    rm netcdf_urls
fi

## Extract
if [ -d $NETCDF_C_DIR ]; then
    rm -r $NETCDF_C_DIR
fi
tar xf $NETCDF_C_TAR
if [ -d $NETCDF_FORTRAN_DIR ]; then
    rm -r $NETCDF_FORTRAN_DIR
fi
tar xf $NETCDF_FORTRAN_TAR

## Load Intel compilers
if [ ! -z `which ifort` ]; then
    source /opt/intel/oneapi/setvars.sh
fi

## Setup the build environment

export CDFROOT=$C_INSTALL_PREFIX
export PATH=$CDFROOT/bin:$PATH
export LD_LIBRARY_PATH=$CDFROOT/lib:$LD_LIBRARY_PATH
export LDFLAGS="-L${CDFROOT}/lib":

mkdir -p $CDFROOT/lib
mkdir -p $CDFROOT/bin
mkdir -p $CDFROOT/include

export CC=`which icx` 
export CXX=`which icpx`
export FC=`which ifx`
export F77=`which ifx`
export F90=`which ifx`
export CPP='icx -E -mcmodel=large' 
export CXXCPP='icpc -E -mcmodel=large' 
export CPPFLAGS="-DNDEBUG -DpgiFortran -I${CDFROOT}/include"

## Optimization flags
export OPTIM="-O3 -mcmodel=large -fPIC -I${CDFROOT}/include"
export CFLAGS=" ${OPTIM}" 
export CXXFLAGS=" ${OPTIM}" 
export FCFLAGS=" ${OPTIM}" 
export F77FLAGS=" ${OPTIM}" 
export F90FLAGS=" ${OPTIM}"
##


## Compile and install netcdf-c
if [ ! `which nc-config` = $CDFROOT/bin/nc-config ]; then
    echo 'compiling netcdf-c'
    mkdir -p $NETCDF_C_DIR/build
    cd $NETCDF_C_DIR/build
    cmake .. -DCMAKE_INSTALL_PREFIX=$C_INSTALL_PREFIX
    make -j
    make install
    cd ../..
else
    echo 'got netcdf-c'
fi

## Compile and install netcdf-fortran
if [ ! `which nf-config` = $CDFROOT/bin/nf-config ]; then
    echo 'compiling netcdf-fortran'
    mkdir -p $NETCDF_FORTRAN_DIR/build
    cd $NETCDF_FORTRAN_DIR/build
    cmake .. -DCMAKE_INSTALL_PREFIX=$FORTRAN_INSTALL_PREFIX
    make -j
    make install
    cd ../..
else
    echo 'got netcdf-fortran'
fi
