# build-netcdf-intel

A bash script to compile NetCDF-C and NetCDF-Fortran with the Intel 
compiler suite.

## Usage

Run the following in a terminal to clone the git repository and run the
script: 
```bash
$ git clone https://git.ecdf.ed.ac.uk/ddekler/build-netcdf-intel
$ cd build-netcdf-intel
$ ./build-netcdf-intel.sh /scratch/$USER/netcdf-intel 
```

This will download the latest NetCDF-C and NetCDF-Fortran source code,
compile it `build` using the Intel compiler and install the libraries
` /scratch/$USER/netcdf-intel`.

The `/scratch/$USER/netcdf-intel` should now contain the following:
 * `bin` with the NetCDF binaries which include the following: 
    `nc-config`, `nf-config` and `ncdump`.
    Add `/scratch/$USER/netcdf-intel/bin` to `PATH` to run them from anywhere.
 * `include` that contains the NetCDF headers (C) and modules (Fortran).
    Add `-I/scratch/$USER/netcdf-intel/include` to your compiler flags to compile 
    against them.
 * `lib` with the NetCDF libraries `libnetcdf.so` and `libnetcdff.so`.
    Add `/scratch/$USER/netcdf-intel/lib` to `LD_LIBRARY_PATH` to link them to a
    binary.
 * `share` with the NetCDF man pages.

Update `PATH`, `LD_PATH` and `LD_LIBRARY_PATH` to include these locations so that your
build process can find the new library.

## TODO

Some improvements to the project:
 * The location of the Intel environment script 
    (`/opt/intel/oneapi/setvars.sh`) hardcoded and might not always be
    in that location.
 * The legacy Intel compilers are used. A choice between legecy and 
    OneAPI might be useful for some.

## Licence

MIT Licerce, see `LICENCE` for details.
